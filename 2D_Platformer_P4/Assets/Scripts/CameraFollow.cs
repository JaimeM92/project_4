﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // Call upon the transform component of target game object
    public Transform target;

    // Speed of the camera that follows player
    public float camSpeed;

    // Places the camerea slightly away from player
    public Vector3 offset;

    private void FixedUpdate()
    {
        // Fixes the camera onto the player while moving
        Vector3 camPosition = target.position + offset;

        // Smooths the motion of camera as it moves
        Vector3 smoothPos = Vector3.Lerp(transform.position, camPosition, camSpeed);
        transform.position = smoothPos;
    }
}
