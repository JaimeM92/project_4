﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerKillPlayer : MonoBehaviour
{
    public string sceneName;

    // On trigger kill player, move to LoseScreen
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            Destroy(other.gameObject);
            SceneManager.LoadScene(sceneName);
        }
    }

}
