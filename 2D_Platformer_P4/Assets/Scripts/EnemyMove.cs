﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    // Variable for enemy speed
    public float enemySpeed;

    // Bool for if enemy is moving in right direction or left
    private bool moveRight = true;

    // Detect ground collider to determine to turn
    public Transform groundDetect;

    private void Update()
    {
        // Move enemy
        transform.Translate(Vector2.right * enemySpeed * Time.deltaTime);

        // Check for ground
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetect.position, Vector2.down, 2f);

        // Move left or right
        if(groundInfo.collider == false)
        {
            if(moveRight == true)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                moveRight = false;
            }

            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                moveRight = true;
            }
        }
    }
}
