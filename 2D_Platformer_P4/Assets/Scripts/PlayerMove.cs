﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    // Player movement speed
    public float playerSpeed;

    // Player jump power
    public float jumpForce;

    private Transform tf;

    private Rigidbody2D rb;

    private AudioSource audio;

    // Number of jumps player can do
    public int maxCount;

    // Distance to read ground collider
    private int distance = 2;

    // Number of jumps player has done
    private int jumpCount;

    // Bool to check if player is in air/ not on ground
    private bool inAir;

    void Start()
    {
        tf = GetComponent<Transform>();

        rb = GetComponent<Rigidbody2D>();

        audio = GetComponent<AudioSource>();

        // Player starts at 0 jump count
        jumpCount = 0;
    }

    void Update()
    {
        // Variable to move on x-axis
        float axisX = Input.GetAxis("Horizontal");

        // Move right
        if (Input.GetKey(KeyCode.D))
        {
            tf.Translate(Vector2.right * playerSpeed * Time.deltaTime);
            tf.localScale = new Vector2(1, 1);
            GetComponent<Animator>().Play("PlayerWalk");
        }

        // Move left
        if(Input.GetKey(KeyCode.A))
        {
            tf.Translate(Vector2.left * playerSpeed * Time.deltaTime);
            tf.localScale = new Vector2(-1, 1);
            GetComponent<Animator>().Play("PlayerWalk");
        }

        // Player jump
        if (Input.GetKeyDown(KeyCode.Space))
        {
            RaycastHit2D ground = Physics2D.Raycast(tf.position, Vector2.down, distance);

            GetComponent<Animator>().Play("PlayerJump");
            audio.Play();

            if (ground.collider != null)
            {
                inAir = false;
                jumpCount = 1;
            }

            else
            {
                inAir = true;
            }

            float jump = Input.GetAxis("Jump");

            if ((jumpCount < maxCount) && (jump > 0))
            {
                jumpCount++;
                rb.AddForce(Vector3.up * jumpForce);
            }
        }
        // Player position at updated checkpoint
        CheckPointManager.instance.UpdateCheckPoint();
    }

    // Player position
    public void PlaceAtPos(Vector3 pos)
    {
        transform.position = pos;
    }
}
