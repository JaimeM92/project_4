﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerKillEnemy : MonoBehaviour
{
    private AudioSource audio;

    private void Start()
    {
        audio = GetComponent<AudioSource>();
    }
    // On trigger kill enemy
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            Destroy(this.gameObject);
            audio.Play();
        }
    }

}
