﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathPitTrigger : MonoBehaviour
{
    // String to call upon the scene
    public string sceneName;

    // When triggered destroy player, move to new scene
    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            Destroy(other.gameObject);
            SceneManager.LoadScene(sceneName);
        }
    }
}
