﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public PlayerMove player;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        else
        {
            Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start ()
    {
        // Load last checkpoint when start game
		CheckPointManager.instance.LoadCheckPoint();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    /*
    void Reset()
    {
        CheckPointManager.instance.Reset();
    }*/
}
