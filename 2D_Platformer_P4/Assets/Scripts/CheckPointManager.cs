﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointManager : MonoBehaviour
{
  
    public static CheckPointManager instance;

    // Variable to call upon for checkpoints
    public int cPoint;

    // Creates list for checkpoints
    public List<GameObject> checkPoints;

    //Initializes Singleton
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start ()
    {
        // Passes through each checkpoint
	    for(int i = 0; i < checkPoints.Count; i++)
        {
            checkPoints[i].SetActive(false);
        }
	}
	
    // Save current checkpoint
	public void SaveCheckPoint()
    {
        PlayerPrefs.SetInt("CheckPoint", cPoint);
    }

    // Load player at last checkpoint
    public void LoadCheckPoint()
    {
        cPoint = PlayerPrefs.GetInt("CheckPoint", 0);
        GameManager.instance.player.PlaceAtPos(checkPoints[cPoint].transform.position);
    }

    // Reset position at checkpoint given
    public void Reset()
    {
        cPoint = 0;
        SaveCheckPoint();
        GameManager.instance.player.PlaceAtPos(checkPoints[cPoint].transform.position);
    }

    // Updates current checkpoint when passing through
    public void UpdateCheckPoint()
    {
        for(int i = 0; i < checkPoints.Count; i++)
        {
            if(GameManager.instance.player.transform.position.x > checkPoints[i].transform.position.x)
            {
                if(i > cPoint)
                {
                    cPoint = i;
                    SaveCheckPoint();
                }
            }
        }
    }
}
